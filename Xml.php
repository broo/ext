<?php

class Ext_Xml
{
    public static function normalize($_name)
    {
        return Ext_String::dash($_name);
    }

    public static function node($_name, $_value = null, $_attrs = null)
    {
        $name = self::normalize($_name);
        $xml = '<' . $name;

        if ($_attrs) {
            foreach ($_attrs as $_key => $_attrValue) {
                if ($_attrValue === '') continue;

                $key = self::normalize($_key);
                $attrValue = str_replace('&', '&amp;', $_attrValue);

                if (strpos($key, 'is-') !== 0) $xml .= " $key=\"$attrValue\"";
                else if ($attrValue)           $xml .= " $key=\"true\"";
            }
        }

        if (empty($_value))         $value = '';
        else if (is_array($_value)) $value = implode($_value);
        else                        $value = $_value;

        if ($value) {
            $value = self::removeControlCharacters($value);
        }

        return $xml . (empty($value) ? ' />' : ">$value</$name>");
    }

    public static function notEmptyNode($_name, $_value = null, $_attrs = null)
    {
        return empty($_value) && empty($_attrs)
             ? ''
             : self::node($_name, $_value, $_attrs);
    }

    public static function encodeCdata($_content)
    {
        return str_replace(
            array('<![CDATA[', ']]>'),
            array('&lt;![CDATA[', ']]&gt;'),
            $_content
        );
    }

    public static function decodeCdata($_content)
    {
        return str_replace(
            array('&lt;![CDATA[', ']]&gt;'),
            array('<![CDATA[', ']]>'),
            $_content
        );
    }

    public static function cdata($_name, $_cdata = null, $_attrs = null)
    {
        $cdata = is_null($_cdata) || $_cdata === ''
               ? null
               : '<![CDATA[' . self::encodeCdata($_cdata) . ']]>';

        return self::node($_name, $cdata, $_attrs);
    }

    public static function notEmptyCdata($_name, $_value = null, $_attrs = null)
    {
        return empty($_value) && empty($_attrs)
             ? ''
             : self::cdata($_name, $_value, $_attrs);
    }

    public static function number($_name, $_number)
    {
        $value = Ext_Number::format(abs($_number));
        if ($_number < 0) $value = '&minus;' . $value;

        return self::cdata($_name, $value, array('value' => $_number));
    }

    /**
     * @param string|array $_container
     * @param string|array $_xml
     */
    public static function append(&$_container, $_xml)
    {
        if ($_xml) {
            if (is_array($_container)) {
                if (is_array($_xml)) $_container = array_merge($_container, $_xml);
                else                 $_container[] = $_xml;

            } else {
                $_container .= is_array($_xml) ? implode($_xml) : $_xml;
            }
        }
    }

    /**
     * Удаление неотображаемых символов (ASCII control characters),
     * используемых в MS Word, которые ломают XML.
     *
     * @link http://www.danshort.com/ASCIImap/indexhex.htm
     * @param string $_source
     * @return string
     */
    public static function removeControlCharacters($_source)
    {
        // Кроме x09, x0A
        return preg_replace("/[\x{7F}\x{00}-\x{08}\x{0B}-\x{1F}]/", '', $_source);
    }

    /**
     * @param string $_xml
     * @return string
     */
    public static function format($_xml)
    {
        return Ext_Dom::getInnerXml(Ext_Dom::get($_xml)->documentElement, true);
    }

    /**
     * @param boolean|string $_dtd
     * @param string $_root
     * @return string
     */
    public static function getHead($_dtd = true, $_root = null)
    {
        $root = empty($_root) ? 'root' : $_root;
        $xml = '<?xml version="1.0" encoding="utf-8"?>';

        if ($_dtd) {
            $dtd = $_dtd === true ? dirname(__FILE__) . '/entities.dtd' : $_dtd;

            if (function_exists('isWindows') && isWindows()) {
                $dtd = 'file:///' . str_replace('\\', '/', $dtd);
            }

            $xml .= PHP_EOL;
            $xml .= "<!DOCTYPE $root SYSTEM \"$dtd\">";
        }

        $xml .= PHP_EOL;
        return $xml;
    }

    /**
     * Передаваемый XML будет помещен внутр корневого элемента $_root
     * с атрибутами $_attrs.
     *
     * @param string $_xml
     * @param string $_attrs
     * @param string $_root
     * @param string|boolean $_dtd
     * @return string
     */
    public static function getDocument($_xml, $_attrs = null, $_root = null, $_dtd = true)
    {
        $root = empty($_root) ? 'root' : $_root;
        return self::getHead($_dtd, $root) . self::node($root, $_xml, $_attrs);
    }

    /**
     * Передаваемый XML должен содерждать корневой элемент.
     *
     * @param string $_xml
     * @param string $_root
     * @param string|boolean $_dtd
     * @return string
     */
    public static function getDocumentForXml($_xml, $_root = null, $_dtd = true)
    {
        $root = empty($_root) ? 'root' : $_root;
        return self::getHead($_dtd, $root) . $_xml;
    }
}
