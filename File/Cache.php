<?php

class Ext_File_Cache
{
    /**
     * @var integer Срок годности неделя.
     */
    const EXPIRE = 604800;

    /**
     * @var boolean
     */
    public static $isEnabled = true;

    /**
     * @var array[array]
     */
    private static $_cache;

    /**
     * @return array[array]
     */
    public static function get()
    {
        if (!self::$isEnabled) return;

        if (is_null(self::$_cache)) {
            self::$_cache = self::load();
        }

        return self::$_cache;
    }

    public static function getDataTable()
    {
        return Ext_Db::get()->getPrefix() . 'file_cache';
    }

    /**
     * @return array[array]
     */
    public static function load()
    {
        if (!self::$isEnabled) return;

        self::cleanExpired();
        $map = array();
        $tbl = self::getDataTable();

        // ORDER_BY-конструкция добавлена, чтобы наверняка брались последние
        // данные по файлу. Уникальность с поля file_path снята, так как
        // часты случаи попытки добавить запись повторно (видимо, из-за
        // одновременных запросов).

        $data = Ext_Db::get()->getList("SELECT * FROM `$tbl` ORDER BY creation_time");

        foreach ($data as $item) {
            $map[$item['file_path']] = $item;
        }

        return $map;
    }

    /**
     * @return resource
     */
    public static function clean()
    {
        if (!self::$isEnabled) return;

        self::$_cache = null;
        return Ext_Db::get()->execute('TRUNCATE ' . self::getDataTable());
    }

    /**
     * @todo Хорошо бы сбрасывать auto_increment счетчики?
     * @return resource
     */
    public static function cleanExpired()
    {
        if (!self::$isEnabled) return;

        self::$_cache = null;

        $tbl = self::getDataTable();
        $past = time() - self::EXPIRE;

        return Ext_Db::get()->execute("DELETE FROM `$tbl` WHERE `creation_time` <= $past");
    }

    /**
     * @param string $_path
     * @return resource|false
     */
    public static function delete($_path)
    {
        if (!self::$isEnabled) return;

        $instance = self::getCache($_path);

        if ($instance) {
            $tbl = self::getDataTable();
            $key = Ext_Db::escape($instance['file_path']);

            unset(self::$_cache[$_path]);
            return Ext_Db::get()->execute("DELETE FROM `$tbl` WHERE `file_path` = $key");

        } else {
            return false;
        }
    }

    /**
     * @param string $_path
     * @return array|false
     */
    public static function getCache($_path)
    {
        if (!self::$isEnabled) return;

        self::get();
        return key_exists($_path, self::$_cache) ? self::$_cache[$_path] : false;
    }

    /**
     * @param string $_filePath
     * @param integer $_size
     * @param integer $_width
     * @param integer $_height
     * @param string $_mime
     * @return array|false
     */
    public static function save($_filePath, $_size, $_width = null, $_height = null, $_mime = null)
    {
        if (!self::$isEnabled) return;

        $size = (int) $_size;
        if ($size == 0) return false;

        $instance = array(
            'size' => $_size,
            'file_path' => $_filePath,
            'creation_time' => time()
        );

        $width = (int) $_width;
        $height = (int) $_height;

        if ($width > 0 && $height > 0) {
            $instance['width'] = $width;
            $instance['height'] = $height;
        }

        if ($_mime) {
            $instance['mime'] = $_mime;
        }

        self::delete($instance['file_path']);

        Ext_Db::get()->execute(
            'INSERT INTO ' . self::getDataTable() .
            Ext_Db::get()->getQueryFields($instance, 'insert')
        );

        $instance[self::getDataTable() . '_id'] = Ext_Db::get()->getLastInsertedId();
        self::$_cache[$instance['file_path']] = $instance;

        return $instance;
    }

    /**
     * @param Ext_File|Ext_Image $_file
     * @return array|false
     */
    public static function saveFile(Ext_File $_file)
    {
        if (!self::$isEnabled) return;

        if ($_file instanceof Ext_Image) {
            return self::save(
                $_file->getPath(),
                $_file->getSize(),
                $_file->getWidth(),
                $_file->getHeight(),
                $_file->getMime()
            );

        } else {
            return self::save($_file->getPath(), $_file->getSize());
        }
    }

    /**
     * @param string $_path
     * @return Ext_File|Ext_Image|false
     */
    public static function getFile($_path)
    {
        if (!self::$isEnabled) return;

        $instance = self::getCache($_path);

        if (
            $instance &&
            !empty($instance['file_path']) &&
            is_file($instance['file_path'])
        ) {
            if (Ext_File::isImageExt(Ext_File::computeExt($instance['file_path']))) {
                $file = new Ext_Image();

                if (!empty($instance['width']) && !empty($instance['height'])) {
                    $file->setWidth($instance['width']);
                    $file->setHeight($instance['height']);
                }

            } else {
                $file = new Ext_File();
            }

            $file->setPath($instance['file_path']);
            $file->setSize($instance['size']);

            if (!empty($instance['mime'])) {
                $file->setMime($instance['mime']);
            }

            return $file;
        }

        return false;
    }
}
