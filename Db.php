<?php

class Ext_Db
{
    /**
     * @var Ext_Mysqli
     */
    protected static $_db;

    /**
     * @return Ext_Mysqli
     */
    public static function get()
    {
        if (!isset(self::$_db)) self::init();
        return self::$_db;
    }

    /**
     * @param Ext_Mysqli $_db
     */
    public static function set(Ext_Mysqli $_db)
    {
        self::$_db = $_db;
    }

    /**
     * @param string $_connectionString mysql://user:password@host/database
     * @throws Exception
     * @return Ext_Mysqli
     */
    public static function init($_connectionString = null)
    {
        global $gDbConnectionString;

        $str = $_connectionString ? $_connectionString : $gDbConnectionString;

        if (empty($str)) {
            throw new Exception('There are no params for connection.');
        }

        self::set(new Ext_Mysqli($str));
        return self::get();
    }

    /**
     * @param string|integer|array $_data
     * @param string $_quote
     * @return string
     */
    public static function escape($_data, $_quote = null)
    {
        return self::get()->escape($_data, $_quote);
    }

    /**
     * @param string $_sourceFile Path to future dump file.
     * @return string
     */
    public static function dump($_sourceFile)
    {
        $user = self::get()->getUser();
        $password = self::get()->getPassword();
        $host = self::get()->getHost();
        $database = self::get()->getDatabase();
        $connectionParams = "-u$user -p$password -h$host";

        if (self::get()->getPort()) {
            $connectionParams .= ' -P' . self::get()->getPort();
        }

        return exec("mysqldump $connectionParams $database > $_sourceFile");
    }
}
