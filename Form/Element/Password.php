<?php

class Ext_Form_Element_Password extends Ext_Form_Element
{
    public function computeValue($_data)
    {
        $value = array();

        if (
            key_exists($this->_name, $_data) &&
            $_data[$this->_name] != ''
        ) {
            $value[$this->_name] = $_data[$this->_name];

            if (key_exists($this->_name . '_check', $_data)) {
                $value[$this->_name . '_check'] = $_data[$this->_name . '_check'];
            }
        }

        if (count($value) > 0) {
            return $value;
        }

        return false;
    }

    public function checkValue($_value = null)
    {
        if (
            $this->isRequired() && (
                empty($_value) ||
                !key_exists($this->_name, $_value) ||
                $_value[$this->_name] == '' ||
                !key_exists($this->_name . '_check', $_value) ||
                $_value[$this->_name . '_check'] == ''
            )
        ) {
            return self::ERROR_REQUIRED;

        } else if (
            empty($_value) || (
                !key_exists($this->_name, $_value) &&
                !key_exists($this->_name . '_check', $_value)
            )
        ) {
            return self::NO_UPDATE;

        } else if (
            !empty($_value) &&
            $_value[$this->_name] != $_value[$this->_name . '_check']
        ) {
            return self::ERROR_SPELLING;

        } else {
            return self::SUCCESS;
        }
    }

    public function getValues()
    {
        return $this->getUpdateStatus() == self::SUCCESS
             ? array($this->_name => $this->getValue($this->_name))
             : false;
    }
}
