<?php

class Ext_Form_Element_CalendarDatetime extends Ext_Form_Element
{
    private $_names = array('date', 'hour', 'minute');

    private function _getPrefixes()
    {
        return array('', $this->getName() . '_', $this->getName() . '-');
    }

    public function getXml()
    {
        $xml = '<hour>';

        for ($i = 0; $i < 24; $i++) {
            $xml .= Ext_Xml::node('item', sprintf('%02d', $i));
        }

        $xml .= '</hour><minute>';

        for ($i = 0; $i < 60; $i = $i + 10) {
            $xml .= Ext_Xml::node('item', sprintf('%02d', $i));
        }

        $xml .= '</minute>';

        $this->addAdditionalXml($xml);

        return parent::getXml();
    }

    public function computeValue($_data)
    {
        if (isset($_data[$this->getName()])) {
            $date = Ext_Date::getDate($_data[$this->getName()]);

            return array('date'   => date('Y-m-d', $date),
                         'hour'   => date('H', $date),
                         'minute' => date('i', $date));

        } else {
            $value = array();

            foreach ($this->_getPrefixes() as $prefix) {
                foreach ($this->_names as $name) {
                    if (isset($_data[$prefix . $name])) {
                        $value[$name] = $_data[$prefix . $name];
                    }
                }

                if (count($value) > 0) {
                    return $value;
                }
            }
        }

        return false;
    }

    public function checkValue($_value = null)
    {
        $value = array();

        foreach ($this->_getPrefixes() as $prefix) {
            foreach ($this->_names as $name) {
                if (!empty($_value[$prefix . $name])) {
                    $value[$name] = $_value[$prefix . $name];
                }
            }
        }

        if (
            $this->isRequired() &&
            count($value) != count($this->_names)
        ) {
            return self::ERROR_REQUIRED;

        } else if (
            empty($value['date']) &&
            empty($value['hour']) &&
            empty($value['minute'])
        ) {
            return self::NO_UPDATE;

        } else if (
            (empty($value['date']) || Ext_Date::getDate($value['date'])) &&
            (int) $value['hour'] < 24 &&
            (int) $value['minute'] < 60
        ) {
            return self::SUCCESS;

        } else {
            return self::ERROR_SPELLING;
        }
    }

    public function getValues()
    {
        if ($this->getUpdateStatus() == self::SUCCESS) {
            $v = $this->getValue();

            if (empty($v['date'])) {
                return array($this->getName() => '');

            } else {
                return array($this->getName() => $v['date'] . ' ' .
                                                 $v['hour'] . ':' .
                                                 $v['minute'] . ':00');
            }

        } else {
            return false;
        }
    }

    public function setValue()
    {
        if (func_num_args() == 1) {
            $arg = func_get_arg(0);

            if (is_array($arg)) {
                $value = $arg;

            } else {
                $arg = Ext_Date::getDate($arg);

                $value = array('date'   => date('Y-m-d', $arg),
                               'hour'   => date('H', $arg),

                               // Приведение минут к ровному счету
                               // (см. возможные значения в getXml в additional/minute/item).
                               'minute' => floor(date('i', $arg) / 10) * 10);
            }

            parent::setValue($value);

        } else {
            $args = func_get_args();
            call_user_func_array(parent::setValue, $args);
        }
    }
}
