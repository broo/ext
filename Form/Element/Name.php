<?php

class Ext_Form_Element_Name extends Ext_Form_Element
{
    public function computeValue($_data)
    {
        $value = array();
        $prefixes = array('', $this->getName() . '_');

        foreach ($prefixes as $prefix) {
            foreach (array('last_name', 'first_name', 'middle_name') as $name) {
                if (isset($_data[$prefix . $name])) {
                    $value[$name] = $_data[$prefix . $name];
                }
            }

            if (count($value) > 0) {
                return $value;
            }
        }

        return false;
    }

    public function checkValue($_value = null)
    {
        if (
            $this->isRequired() &&
            (empty($_value['first_name']) || empty($_value['last_name']))
        ) {
            return self::ERROR_REQUIRED;

        } else if (
            !isset($_value['first_name']) &&
            !isset($_value['last_name']) &&
            !isset($_value['middle_name'])
        ) {
            return self::NO_UPDATE;

        } else {
            return self::SUCCESS;
        }
    }

    public function getValues()
    {
        if ($this->getUpdateStatus() == self::SUCCESS) {
            return $this->getValue();

        } else {
            return false;
        }
    }
}
