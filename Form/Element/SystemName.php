<?php

class Ext_Form_Element_SystemName extends Ext_Form_Element
{
    public function checkValue($_value = null)
    {
        $status = parent::checkValue($_value);

        if (
            $status == self::SUCCESS &&
            $_value != '' &&
            !Ext_File::checkName($_value)
        ) {
            return self::ERROR_SPELLING;
        }

        return $status;
    }
}
